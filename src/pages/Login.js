import { useEffect, useState } from "react"
import { Form, Row, Col, Container, Button } from "react-bootstrap"

export default function Login (){
    const [email, setEmail] = useState ("")
    const [pw, setPw] = useState ("")
    const [isDisabled, setIsDisabled] = useState(true)

    useEffect(() => {

        if(email !== "" && pw !== "") {
            setIsDisabled(false)
        }else{
            setIsDisabled(true)
        }        
    }, [email,pw])
    
    const loginUser = (e) => {
        alert('You are now logged in.')

        // fetch(`http://localhost:4005/api/users/login`, {
		// 	method: "POST",
		// 	headers: {
		// 		"Content-Type": "application/json"
		// 	},
		// 	body: JSON.stringify({
		// 		email: email,
		// 		password: pw
		// 	})
		// }).then(result => result.json())
		// .then(result => {
        //     if(result){

        //         localStorage.setItem('token', result.token)
        //         let token = localStorage.getItem('token')

        //         fetch(`http://localhost:4005/api/users/profile`, {
		// 			method: "GET",
		// 			headers:{
		// 				"Authorization": `Bearer ${token}`
		// 			}
		// 		}).then(result => result.json())
		// 		.then(result => {
		// 			localStorage.setItem('id', result._id)
		// 			localStorage.setItem('admin', result.isAdmin)

		// 			alert('You are now logged in.')
		// 		})
        //     }else {
		// 		alert('Something went wrong. Please try again.')
		// 	}
        // })
    }
    return (
        <Container className="m-5">
            <h3 className="text-center">Log In</h3>
            <Row className="justify-content-center">
                <Col xs={12} md={6}>
                    <Form onSubmit={(e) => loginUser(e) }>
                        <Form.Group className="mb-3">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                            type="text" 
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                            type="password" 
                            value={pw}
                            onChange={(e) => setPw(e.target.value)}
                            />
                        </Form.Group>

                        <Button 
                        variant="info" 
                        type="submit"
                        disabled={isDisabled}
                        >
                        Submit</Button>
                    </Form>
                </Col>
            </Row>
        </Container>)
}