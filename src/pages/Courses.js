import CourseCard from "./../components/CourseCard";
import { Fragment } from "react";
import coursesData from "./../mockData/courses";

export default function Courses(){

    const courses = coursesData.map(course => {
        return <CourseCard key={course.id} courseProp = {course}/> 
    })

    return (<Fragment>{courses}</Fragment>)
}