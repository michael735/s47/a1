import {Fragment} from 'react';
//import Home from './pages/Home';
//import Courses from './pages/Courses';
//import Register from './pages/Register';
import AppNavbar from './components/AppNavbar';
import Login from './pages/Login';

function App() {

  return(
    <Fragment>
      <AppNavbar/>
      {/* <Home/>  */}
      {/* <Courses/> */}
      {/* <Register/> */}
      <Login />
    </Fragment>    
  )
}
    

export default App;
